package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class regtangulo extends AppCompatActivity {
    private RegtanguloActivity RegtanguloActivity;
    private TextView Idcliente;
    private TextView tparea;
    private TextView idperimetros;
    private EditText Tbase;
    private EditText Taltura;
    private Button btnReg;
    private Button btnCal;
    private Button btnLim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regtangulo);
        Idcliente = (TextView) findViewById(R.id.lblCliente);
        btnReg = (Button) findViewById(R.id.btnSalir);
        btnCal = (Button) findViewById(R.id.btnCalcular);
        btnLim = (Button) findViewById(R.id.btnLimpiar);
        Tbase = (EditText) findViewById(R.id.txtBase);
        Taltura = (EditText) findViewById(R.id.txtDAltura);
        tparea = (TextView) findViewById(R.id.lblArea);
        idperimetros = (TextView) findViewById(R.id.lblPerimetro);
        RegtanguloActivity =  new RegtanguloActivity();

        //Sacar el dato String
        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("cliente");
        Idcliente.setText("Cliente: " + cliente);

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Tbase.getText().toString().matches("")){
                    Toast.makeText(regtangulo.this,"Debes llenar todos los campos",Toast.LENGTH_SHORT).show();
                }
                else if(Taltura.getText().toString().matches("")){
                    Toast.makeText(regtangulo.this,"Debes llenar todos los campos",Toast.LENGTH_SHORT).show();
                }
                else{
                    String bas = Tbase.getText().toString();
                    float base = Float.parseFloat(bas);
                    String alt = Taltura.getText().toString();
                    float altura = Float.parseFloat(alt);

                    RegtanguloActivity.setBase(base);
                    RegtanguloActivity.setAltura(altura);

                    String area = String.valueOf(RegtanguloActivity.calcularArea());
                    tparea.setText("Area: " + area);
                    String perimetro = String.valueOf(RegtanguloActivity.calcularPerimetro());
                    idperimetros.setText("Perimetro: " + perimetro);
                }
            }
        });

        btnLim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tbase.setText("");
                Taltura.setText("");
                tparea.setText("");
                idperimetros.setText("");
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
